Pod::Spec.new do |s|
  s.name         = "CLKit"
  s.version      = "1.0.24"
  s.summary      = "This is my kit."
  s.description  = "我的iOS常用代码-------------------------------------------------"
  s.homepage     = "https://gitlab.com/dashboard/snippets"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "plus" => "luochao_p@126.com" }
  # Or just: s.author    = "plus"
  # s.authors            = { "plus" => "luochao_p@126.com" }
  # s.social_media_url   = "http://twitter.com/plus"
  s.platform     = :ios
  s.platform     = :ios, "8.0"
  #  When using multiple platforms
  # s.ios.deployment_target = "8.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"
  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/*"
  s.source       = { :git => "https://gitlab.com/anqila/CLKit.git", :tag => s.version }
  # s.public_header_files = "Classes/**/*.h"

  # s.resource  = "Classes/CLKit/MVP/CLEmptyDataSource.bundle"
  # s.resources = ["xx.png"]
  s.resources = ['Classes/Assets/CLEmptyDataSource.bundle' , 'Classes/Assets/CLSearchExtension.bundle']

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"

  # s.framework  = "SomeFramework"
  s.frameworks = 'Foundation', 'UIKit', 'CoreData', 'QuartzCore', 'CoreLocation', 'MapKit'

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"

  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  s.dependency "AFNetworking"
  s.dependency "SVProgressHUD"
  s.dependency "DZNEmptyDataSet"
  s.dependency "ReactiveObjC"
  s.dependency "Masonry"
  s.dependency "YYModel"
  s.dependency "MJRefresh"
  s.dependency "DateTools"
  
end
